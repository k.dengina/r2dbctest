package com.dengina.r2dbctest.cache;

import com.dengina.r2dbctest.core.Person;
import com.github.benmanes.caffeine.cache.AsyncCacheLoader;
import com.github.benmanes.caffeine.cache.AsyncLoadingCache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.Expiry;
import lombok.extern.log4j.Log4j2;
import org.checkerframework.checker.index.qual.NonNegative;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;


@Component
@Log4j2
public class PersonCaffeineService {

    @Autowired
    private LbsClient client;

    AsyncLoadingCache<Integer, Person> caffeineCache =
            Caffeine.newBuilder()
                    .expireAfter(new Expiry<Integer, Person>() {
                        @Override
                        public long expireAfterCreate(@NonNull Integer key, @NonNull Person value, long currentTime) {
                            return Duration.ofSeconds(5).toNanos();
                        }

                        @Override
                        public long expireAfterUpdate(@NonNull Integer key, @NonNull Person value, long currentTime, @NonNegative long currentDuration) {
                            return 0;
                        }

                        @Override
                        public long expireAfterRead(@NonNull Integer key, @NonNull Person value, long currentTime, @NonNegative long currentDuration) {
                            return 0;
                        }
                    })
                    .buildAsync(new AsyncCacheLoader<Integer, Person>() {
                        @Override
                        public @NonNull CompletableFuture<Person> asyncLoad(@NonNull Integer key, @NonNull Executor executor) {
                            return client.getPerson(key).toFuture();
                        }
                    });

    public Mono<Person> caffeineSearch(Integer key) {
        log.info("From cache " + key);
        return Mono.fromFuture(caffeineCache.get(key));
    }
}
