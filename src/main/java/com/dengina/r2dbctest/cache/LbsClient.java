package com.dengina.r2dbctest.cache;

import com.dengina.r2dbctest.core.Person;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.LocalTime;

@Component
@Log4j2
public class LbsClient {
    public Mono<Person> getPerson(Integer key) {
        log.info("From lbs " + key);
        return Mono
                .just(new Person(key.longValue(), "name" + key, LocalTime.now().getNano()))
                .delayElement(Duration.ofSeconds(5));
    }

}
