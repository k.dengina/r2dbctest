package com.dengina.r2dbctest.cache;

import com.dengina.r2dbctest.core.Person;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.cache.CacheMono;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Signal;

import java.util.Optional;


@Log4j2
@Component
public class PersonEhCacheService {

    @Autowired
    private CacheHelper ehCache;

    @Autowired
    private LbsClient client;

    public Mono<Person> ehCacheSearch(Integer key) {
        return CacheMono.lookup(k ->
                    Mono.justOrEmpty(ehCache.get(k))
                            .map(Signal::next),
                key)
                .onCacheMissResume(client.getPerson(key))
                .andWriteWith((k, sig) ->
                        Mono.fromRunnable(
                                () -> Optional.ofNullable(sig.get())
                                        .ifPresent(v -> ehCache.put(k, v)))
                );
    }

}
