package com.dengina.r2dbctest.cache;

import com.dengina.r2dbctest.core.Person;
import lombok.extern.log4j.Log4j2;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.ExpiryPolicy;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.function.Supplier;

@Component
@Log4j2
public class CacheHelper {

    private CacheManager cacheManager;
    private Cache<Integer, Person> personCache;

    public CacheHelper() {
        cacheManager = CacheManagerBuilder
                .newCacheManagerBuilder()
                .build();
        cacheManager.init();

        personCache = cacheManager
                .createCache("person", CacheConfigurationBuilder
                        .newCacheConfigurationBuilder(
                                Integer.class, Person.class,
                                ResourcePoolsBuilder.heap(10))
                .withExpiry(new ExpiryPolicy<Integer, Person>() {
                    @Override
                    public Duration getExpiryForCreation(Integer key, Person value) {
                        return Duration.ofSeconds(5);
                    }

                    @Override
                    public Duration getExpiryForAccess(Integer key, Supplier<? extends Person> value) {
                        return null;
                    }

                    @Override
                    public Duration getExpiryForUpdate(Integer key, Supplier<? extends Person> oldValue, Person newValue) {
                        return null;
                    }
                }));
    }

    public Person get(Integer key) {
        log.info("Get Person from cache " + key);
        return getPersonFromCacheManager().get(key);
    }

    public void put(Integer key, Person p) {
        log.info("Put Person to cache " + key);
        getPersonFromCacheManager().put(key, p);
    }

    private Cache<Integer, Person> getPersonFromCacheManager() {
        return cacheManager.getCache("person", Integer.class, Person.class);
    }

}
