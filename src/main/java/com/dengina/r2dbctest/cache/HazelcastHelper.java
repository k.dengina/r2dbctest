package com.dengina.r2dbctest.cache;

import com.dengina.r2dbctest.core.Person;
import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import org.springframework.stereotype.Component;

@Component
public class HazelcastHelper {


    private HazelcastInstance hzInstance;


    public HazelcastHelper() {
        hzInstance = Hazelcast.newHazelcastInstance(hazelCastConfig());
    }

    public IMap<Integer, Person> personCache() {
        return hzInstance.getMap("PERSON");
    }


    private Config hazelCastConfig() {
        //config cache setup
        Config config = new Config();
        config.setInstanceName("EXAMPLE-CACHE");

        MapConfig ourCache = new MapConfig();
        ourCache.setTimeToLiveSeconds(5);

        ourCache.setEvictionPolicy(EvictionPolicy.LFU);
        config.getMapConfigs().put("PERSON", ourCache);

        return config;
    }
}
