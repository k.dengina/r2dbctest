package com.dengina.r2dbctest.cache;

import com.dengina.r2dbctest.core.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.cache.CacheMono;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Signal;

@Component
public class PersonHazelcastService {

    @Autowired
    protected HazelcastHelper helper;

    @Autowired
    private LbsClient client;


    public Mono<Person> searchPerson(Integer key) {
        return CacheMono
                .lookup(k -> Mono.justOrEmpty(helper.personCache().get(key)).map(Signal::next), key)
                .onCacheMissResume(client.getPerson(key))
                .andWriteWith((k, sig) -> Mono.fromRunnable(() ->
                        writeCacheValue(key, sig.get())));
    }


    private Mono<Person> writeCacheValue(Integer key, Person data) {
        if(data != null) {
            helper.personCache().set(key, data);
            return Mono.just(data);
        }
        return Mono.empty();
    }

}
