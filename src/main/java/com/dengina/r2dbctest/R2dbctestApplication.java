package com.dengina.r2dbctest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class R2dbctestApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(R2dbctestApplication.class, args);
	}
}
