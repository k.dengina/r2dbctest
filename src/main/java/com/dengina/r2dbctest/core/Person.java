package com.dengina.r2dbctest.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
@AllArgsConstructor
public class Person {
    @Id
    private Long id;
    private String name;
    private int age;
}
