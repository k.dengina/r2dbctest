package com.dengina.r2dbctest.core;

import com.dengina.r2dbctest.core.Person;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface PersonRepository extends ReactiveCrudRepository<Person, Long> {
}
