package com.dengina.r2dbctest;

import com.dengina.r2dbctest.config.R2dbcConfiguration;
import com.dengina.r2dbctest.config.TestConfiguration;
import com.dengina.r2dbctest.core.Person;
import com.dengina.r2dbctest.core.PersonRepository;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.r2dbc.core.DatabaseClient;
import org.testcontainers.junit.jupiter.Testcontainers;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.concurrent.TimeUnit;

@SpringBootTest(classes = {TestConfiguration.class, R2dbcConfiguration.class})
@Testcontainers
@Log4j2
public class R2dbcReadTableTests {

	@Autowired
	private PersonRepository repository;

	private final static Integer countRows = 130000000;

	@Test
	public void testOutOfMemory() {
		Assertions.assertThrows(OutOfMemoryError.class, () -> {
			final Person[] personArray = new Person[countRows];
		});
	}

	@Test
	public void testReadWrite() {
		repository.saveAll(personFlux(countRows))
				.doOnNext(i -> {
					log.info("Insert person: " + i);
					try {
						TimeUnit.MILLISECONDS.sleep(2);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				})
				.doOnComplete(() -> log.info("Add all persons..."))
				.as(StepVerifier::create)
				.expectNextCount(countRows)
				.verifyComplete();

		repository.findAll()
				.doOnNext(i ->  {
						log.info("Find person: " + i);
						try {
							TimeUnit.MILLISECONDS.sleep(5);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				)
				.doOnComplete(() -> log.info("Find all persons..."))
				.as(StepVerifier::create)
				.expectNextCount(countRows)
				.verifyComplete();
	}

	private Flux<Person> personFlux(int count) {
		return Flux.range(1, count)
				.map(i -> new Person(null, "name" + i, i));
	}

}
