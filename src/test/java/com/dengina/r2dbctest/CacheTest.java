package com.dengina.r2dbctest;

import com.dengina.r2dbctest.cache.PersonCaffeineService;
import com.dengina.r2dbctest.cache.PersonEhCacheService;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@Log4j2
@SpringBootTest
@RunWith(SpringRunner.class)
public class CacheTest {

    @Autowired
    private PersonEhCacheService cacheService;

    @Autowired
    private PersonCaffeineService caffeineService;

    @Test
    public void HazelcastCache() throws InterruptedException {
        log.info("Hazelcast ----");
        caffeineService.caffeineSearch(1)
                .doOnNext(log::info)
                .subscribe();

        caffeineService.caffeineSearch(1)
                .doOnNext(log::info)
                .subscribe();
        Thread.sleep(6000);

        caffeineService.caffeineSearch(1)
                .doOnNext(log::info)
                .subscribe();

        Thread.sleep(6000);

        caffeineService.caffeineSearch(1)
                .doOnNext(log::info)
                .block();
        Thread.sleep(6000);
    }

    @Test
    public void ehCacheTest() throws InterruptedException {
        log.info("ehCache ----");
        cacheService.ehCacheSearch(1)
                .doOnNext(log::info)
                .subscribe();

        cacheService.ehCacheSearch(1)
                .doOnNext(log::info)
                .subscribe();

        Thread.sleep(5000);

        cacheService.ehCacheSearch(1)
                .doOnNext(log::info)
                .subscribe();

        Thread.sleep(2000);

        cacheService.ehCacheSearch(1)
                .doOnNext(log::info)
                .block();
        Thread.sleep(6000);
    }

    @Test
    public void caffeineTest() throws InterruptedException {
        log.info("caffeine ----");
        caffeineService.caffeineSearch(1)
                .doOnNext(log::info)
                .subscribe();

        caffeineService.caffeineSearch(1)
                .doOnNext(log::info)
                .subscribe();

        Thread.sleep(6000);

        caffeineService.caffeineSearch(1)
                .doOnNext(log::info)
                .subscribe();

        Thread.sleep(6000);

        caffeineService.caffeineSearch(1)
                .doOnNext(log::info)
                .block();

    }
}
