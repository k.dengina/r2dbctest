package com.dengina.r2dbctest.config;


import com.dengina.r2dbctest.config.R2dbcConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.testcontainers.containers.PostgreSQLContainer;


@Configuration
@AutoConfigureBefore(value = R2dbcConfiguration.class)
public class TestConfiguration {

    @Bean(initMethod = "start", name = "container")
    public PostgreSQLContainer container() {
        return new PostgreSQLContainer("postgres:12");
    }

    @Bean("configuration")
    @DependsOn("container")
    public PostgresqlConnectionConfiguration configuration(PostgreSQLContainer container) {
        return PostgresqlConnectionConfiguration.builder()
                .port(container.getMappedPort(PostgreSQLContainer.POSTGRESQL_PORT))
                .host(container.getContainerIpAddress())
                .database(container.getDatabaseName())
                .username(container.getUsername())
                .password(container.getPassword())
                .build();
    }

}
